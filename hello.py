print('Hello world')


#Tipos basicos de datos
# Int = entero
#float= real
#str= cadena de caracteres
#bool= True or false

a = 4
b = 3.6
c ='Catalina'
d = False
print(a)
print(b)
print(c)
print(d)

tipo_a = type(a)
print(tipo_a) 
#obtiene el tipo de dato de la variable
print(type(b))

x =2 
print(type(x))
x =2.5 
print(type(x))
x ='Catalina'
print(type(x))
